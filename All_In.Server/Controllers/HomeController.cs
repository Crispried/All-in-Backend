﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using System.Net;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace All_In.Server.Controllers
{
    [Route("api/home")]
    [EnableCors("CorsPolicy")]
    public class HomeController : Controller
    {
        // POST api/values
        [HttpPost("contact")]
        public void Contact([FromBody]ContactVM request)
        {
            try
            {
                var email = "allrecruitingin@gmail.com";
                var password = "ALLin2020";

                var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential(email, password),
                    EnableSsl = true
                };

                var subject = string.Format($"Новый лид {request.Company}");

                var mailMessage = new MailMessage
                {
                    Subject = subject,
                    From = new MailAddress(request.Email),
                    IsBodyHtml = true
                };
                mailMessage.To.Add(email);

                mailMessage.Body = string.Format($"<p>{ request.Message }</p> <p>{ request.Company }</p> <p>{ request.NIP }</p> <p>{ request.Phone }</p>");

                client.Send(mailMessage);
            }
            catch (Exception e)
            {

            }
        }
    }

    public class ContactVM
    {
        public string Company { get; set; }

        public string NIP { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }
    }
}
