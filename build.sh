echo 'Login'
sudo docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY

echo 'Build the project'
sudo docker build -t builder -f BuilderDockerfile .

echo 'Build the deployment containers'
sudo docker build -t $CI_REGISTRY_IMAGE/server:dev -f ApiDockerfile . & 
wait

echo 'Push the new images'
sudo docker push $CI_REGISTRY_IMAGE/server:dev &
wait
