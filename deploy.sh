echo 'Login'
sudo docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

echo 'Pull the images'
sudo docker pull $CI_REGISTRY_IMAGE/server:dev

echo 'Remove the old containers'
sudo docker stop server || true && sudo docker rm server -f || true

echo 'Cleanup the old images'
sudo docker rmi `sudo docker images -aq`

echo 'Free the storage'
sudo docker rmi -f $(sudo docker images -aq --filter dangling=true)

echo 'Run the containers'
sudo docker run --name=server -p 5000:5000 -itd $CI_REGISTRY_IMAGE/server:dev
